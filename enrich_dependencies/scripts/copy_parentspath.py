import argparse
import os
import sys
import time

import pandas as pd
import numpy as np
import requests
import json
from datetime import datetime
from pygount import SourceAnalysis, ProjectSummary
from glob import glob
from pathlib import Path
from elasticsearch import Elasticsearch

def main():
    args = parse_args()
    
    # Opening JSON file
    correctf = open(args.correct)
    correctpp = json.load(correctf)
    
    # Opening JSON file
    wrongf = open(args.wrong)
    wrongpp = json.load(wrongf)

    deplist = copygoodpp(correctpp, wrongpp)
    with open(args.output, "w") as outfile:
        json.dump(deplist, outfile)
    

def parse_args():
    parser = argparse.ArgumentParser(usage="usage: enrich_dependencies.py [options]",
                                     description="Enrich metrics for representing the tree hierarchy")
    parser.add_argument("-correct", "--correct", required=True,
                        help="Dependencies list file with corrent ParentsPath", default="./dependencieslist_correct.json")
    parser.add_argument("-wrong", "--wrong", required=True,
                        help="Dependencies list file with wrong ParentsPath", default="./dependencieslist_wrong.json")
    parser.add_argument("-o", "--output", required=True,
                        help="Dependencies list file output with the right mix", default="./dependencieslist_fixed.json")
    
    return parser.parse_args()


def copygoodpp(good, wrong):
    for depg in good:
        for depw in wrong:
            depw['dependencies'] = []
            if depg['parentsPath'].startswith(depw['parentsPath']) and depg['id'] == depw['id']:
                depw['parentsPath'] = depg['parentsPath']
                break
       
    return wrong


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        s = "\n\nReceived Ctrl-C or other break signal. Exiting.\n"
        sys.stdout.write(s)
        sys.exit(0)

