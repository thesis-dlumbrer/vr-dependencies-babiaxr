import argparse
import os
import sys
import time

import pandas as pd
import numpy as np
import requests
import json
from datetime import datetime
from dateutil.relativedelta import relativedelta
from pygount import SourceAnalysis, ProjectSummary
from glob import glob
from pathlib import Path
from elasticsearch import Elasticsearch


ES_HOST = "http://localhost:9200"
ES_INDEX = "git_enrich"
COMMUNITY_QUERY = {
  "aggs": {
    "repos": {
      "terms": {
        "field": "github_repo",
        "size": 500000,
        "order": {
          "max_commit_date": "desc"
        }
      },
      "aggs": {
        "lines_added": {
          "sum": {
            "field": "lines_added"
          }
        },
        "lines_changed": {
          "sum": {
            "field": "lines_changed"
          }
        },
        "lines_removed": {
          "sum": {
            "field": "lines_removed"
          }
        },
        "commiters": {
          "cardinality": {
            "field": "author_uuid"
          }
        },
        "max_commit_date": {
          "max": {
            "field": "commit_date"
          }
        },
        "min_commit_date": {
          "min": {
            "field": "commit_date"
          }
        },
        "files": {
          "sum": {
            "field": "files"
          }
        },
        "commits": {
          "cardinality": {
            "field": "hash"
          }
        }
      }
    }
  },
  "size": 0,
  "_source": {
    "excludes": []
  },
  "stored_fields": [
    "*"
  ],
  "script_fields": {
    "painless_inverted_lines_removed_git": {
      "script": {
        "source": "return doc['lines_removed'].value * -1",
        "lang": "painless"
      }
    }
  },
  "docvalue_fields": [
    {
      "field": "author_date",
      "format": "date_time"
    },
    {
      "field": "commit_date",
      "format": "date_time"
    },
    {
      "field": "demography_max_date",
      "format": "date_time"
    },
    {
      "field": "demography_min_date",
      "format": "date_time"
    },
    {
      "field": "grimoire_creation_date",
      "format": "date_time"
    },
    {
      "field": "metadata__enriched_on",
      "format": "date_time"
    },
    {
      "field": "metadata__timestamp",
      "format": "date_time"
    },
    {
      "field": "metadata__updated_on",
      "format": "date_time"
    },
    {
      "field": "utc_author",
      "format": "date_time"
    },
    {
      "field": "utc_commit",
      "format": "date_time"
    }
  ],
  "query": {
    "bool": {
      "must": [
        {
          "match_all": {}
        },
        {
          "match_all": {}
        },
        {
          "range": {
            "grimoire_creation_date": {
              "gte": 1348142971607,
              "lte": 1663660709792,
              "format": "epoch_millis"
            }
          }
        }
      ],
      "filter": [],
      "should": [],
      "must_not": []
    }
  }
}

def main():
    args = parse_args()
    
    # Opening JSON file
    correctf = open(args.correct)
    correctpp = json.load(correctf)
    
    # Opening JSON file
    wrongf = open(args.wrong)
    wrongpp = json.load(wrongf)
    
    #a = []
    #for dep in wrongpp:
    #    if 'pkg_age' not in dep:
    #        a.append(dep['repository'])
    # Export projects.json for grimoirelab docker
    #projectsjson = createprojectjson(a)
    #with open("./pjsonaux.json", "w") as outfile:
    #    json.dump(projectsjson, outfile)
    
    
    # deplist = copygoodpp(correctpp, wrongpp, args.github_username, args.github_token)
    
    deplist = correctpp
    add_community_from_es(deplist)
    
    annotateduplicates(deplist)
    
    with open(args.output, "w") as outfile:
        json.dump(deplist, outfile)


def parse_args():
    parser = argparse.ArgumentParser(usage="usage: enrich_dependencies.py [options]",
                                     description="Enrich metrics for representing the tree hierarchy")
    parser.add_argument("-correct", "--correct", required=True,
                        help="Dependencies list file with corrent ParentsPath",
                        default="./dependencieslist_correct.json")
    parser.add_argument("-wrong", "--wrong", required=True,
                        help="Dependencies list file with wrong ParentsPath", default="./dependencieslist_wrong.json")
    parser.add_argument("-o", "--output", required=True,
                        help="Dependencies list file output with the right mix",
                        default="./dependencieslist_fixed.json")
    parser.add_argument("-githubusername", "--github-username", required=True,
                        help="GitHub username for fetching the API",
                        default="dlumbrer")
    parser.add_argument("-githubtoken", "--github-token", required=True,
                        help="GitHub token for fetching the API",
                        default="xxxxx")
    
    return parser.parse_args()


def copygoodpp(good, wrong, username, token):
    for depg in good:
        for depw in wrong:
            if depg['id'] == depw['id']:
                print("Vamos a rellenar este! {}".format(depw['id']))
                # If age, if not call GitHub and ElasticSearch
                if 'pkg_age' in depg and depg['pkg_age'] != 0:
                    depw['pkg_age'] = depg['pkg_age']
                    depw['last_act'] = depg['last_act']
                    depw['commits_alltime'] = depg['commits_alltime']
                    depw['commiters_alltime'] = depg['commiters_alltime']
                    depw['commits_lastyear'] = depg['commits_lastyear']
                    depw['commiters_lastyear'] = depg['commiters_lastyear']
                    depw['commits_last6m'] = depg['commits_last6m']
                    depw['commiters_last6m'] = depg['commiters_last6m']
                    depw['nvuln'] = depg['nvuln']
                    depw['nvuln_critic'] = depg['nvuln_critic']
                    depw['nvuln_high'] = depg['nvuln_high']
                    depw['nvuln_mod'] = depg['nvuln_mod']
                    depw['nvuln_low'] = depg['nvuln_low']
                    depw['nvuln_info'] = depg['nvuln_info']
                    depw['nprs'] = depg['alltime_nprs']
                    depw['nprs_closed'] = depg['alltime_nprs_closed']
                    depw['nprs_open'] = depg['alltime_nprs_open']
                    depw['nissues'] = depg['alltime_nissues']
                    depw['nissues_closed'] = depg['alltime_nissues_closed']
                    depw['nissues_open'] = depg['alltime_nissues_open']
                    depw['nprs_lastyear'] = depg['nprs']
                    depw['nprs_closed_lastyear'] = depg['nprs_closed']
                    depw['nissues_lastyear'] = depg['nissues']
                    depw['nissues_closed_lastyear'] = depg['nissues_closed']
                else:
                    print("GitHUB API going for {}".format(depw['id']))
                    githubapi_nopkgage(depw, username, token)
    
    return wrong

def githubapi_nopkgage(dep, username, token):
    when = (datetime.now() - relativedelta(years=1))
    now = datetime.now()
    
    # Check Rate Limit
    headers = {"Accept": "application/vnd.github+json", "Authorization": "Bearer {}".format(token)}
    x = requests.get('https://api.github.com/rate_limit', headers=headers)
    if x.status_code == 200:
        resp = x.json()
        if resp['resources']['search']['remaining'] < 10:
            print("RATE LIMIT EXCEEDED, SLEEPING FOR 60 seconds")
            time.sleep(60)
            print("LETS TRY NOW!")
    else:
        print("Something wrong happened", x.status_code)
        exit(1)
    
    # Number of PRs
    x = requests.get(
        'https://api.github.com/search/issues?q=repo:{}%20is:pr%20created:{}-{}-{}..{}-{}-{}&per_page=1'.format(
            dep['repository'].split("github.com/")[-1], when.year, when.month, when.day, now.year, now.month, now.day),
        auth=(username, token))
    if x.status_code == 200:
        resp = x.json()
        dep['nprs_lastyear'] = resp['total_count']
    else:
        print("Something wrong happened", x.status_code)
    
    x = requests.get(
        'https://api.github.com/search/issues?q=repo:{}%20is:pr&per_page=1'.format(
            dep['repository'].split("github.com/")[-1], when.year, when.month, when.day, now.year, now.month,
            now.day), auth=(username, token))
    if x.status_code == 200:
        resp = x.json()
        dep['nprs'] = resp['total_count']
    else:
        print("Something wrong happened", x.status_code)
    
    # Number of Closed PRs
    x = requests.get(
        'https://api.github.com/search/issues?q=repo:{}%20is:pr%20is:closed%20created:{}-{}-{}..{}-{}-{}&per_page=1'.format(
            dep['repository'].split("github.com/")[-1], when.year, when.month, when.day, now.year, now.month, now.day),
        auth=(username, token))
    if x.status_code == 200:
        resp = x.json()
        dep['nprs_closed_lastyear'] = resp['total_count']
    else:
        print("Something wrong happened", x.status_code)
    
    x = requests.get(
        'https://api.github.com/search/issues?q=repo:{}%20is:pr%20is:closed&per_page=1'.format(
            dep['repository'].split("github.com/")[-1], when.year, when.month, when.day, now.year, now.month,
            now.day), auth=(username, token))
    if x.status_code == 200:
        resp = x.json()
        dep['nprs_closed'] = resp['total_count']
    else:
        print("Something wrong happened", x.status_code)
    
    # Number of Open PRs
    x = requests.get(
        'https://api.github.com/search/issues?q=repo:{}%20is:pr%20is:open%20created:{}-{}-{}..{}-{}-{}&per_page=1'.format(
            dep['repository'].split("github.com/")[-1], when.year, when.month, when.day, now.year, now.month, now.day),
        auth=(username, token))
    if x.status_code == 200:
        resp = x.json()
        dep['nprs_open'] = resp['total_count']
    else:
        print("Something wrong happened", x.status_code)
    
    # Number of Issues
    x = requests.get(
        'https://api.github.com/search/issues?q=repo:{}%20is:issue%20created:{}-{}-{}..{}-{}-{}&per_page=1'.format(
            dep['repository'].split("github.com/")[-1], when.year, when.month, when.day, now.year, now.month, now.day),
        auth=(username, token))
    if x.status_code == 200:
        resp = x.json()
        dep['nissues_lastyear'] = resp['total_count']
    else:
        print("Something wrong happened", x.status_code)
    
    x = requests.get('https://api.github.com/search/issues?q=repo:{}%20is:issue&per_page=1'.format(
        dep['repository'].split("github.com/")[-1]), auth=(username, token))
    if x.status_code == 200:
        resp = x.json()
        dep['nissues'] = resp['total_count']
    else:
        print("Something wrong happened", x.status_code)
    
    # Number of Closed Issues
    x = requests.get(
        'https://api.github.com/search/issues?q=repo:{}%20is:issue%20is:closed%20created:{}-{}-{}..{}-{}-{}&per_page=1'.format(
            dep['repository'].split("github.com/")[-1], when.year, when.month, when.day, now.year, now.month, now.day),
        auth=(username, token))
    if x.status_code == 200:
        resp = x.json()
        dep['nissues_closed_lastyear'] = resp['total_count']
    
    x = requests.get(
        'https://api.github.com/search/issues?q=repo:{}%20is:issue%20is:closed&per_page=1'.format(
            dep['repository'].split("github.com/")[-1], when.year, when.month, when.day, now.year, now.month,
            now.day), auth=(username, token))
    if x.status_code == 200:
        resp = x.json()
        dep['nissues_closed'] = resp['total_count']
    
    # Number of Open Issues
    x = requests.get(
        'https://api.github.com/search/issues?q=repo:{}%20is:issue%20is:open%20created:{}-{}-{}..{}-{}-{}&per_page=1'.format(
            dep['repository'].split("github.com/")[-1], when.year, when.month, when.day, now.year, now.month, now.day),
        auth=(username, token))
    if x.status_code == 200:
        resp = x.json()
        dep['nissues_open'] = resp['total_count']
    else:
        print("Something wrong happened", x.status_code)


def add_community_from_es(deplist):
    
    print("STARTING COMMUNITY ANALYSIS FROM ELASTICSEARCH AGG QUERY")
    COMMUNITY_QUERY["query"]["bool"]["must"][2]["range"]["grimoire_creation_date"]["lte"] = int(
        datetime.now().timestamp() * 1000)
    es = Elasticsearch([ES_HOST])
    res = es.search(index=ES_INDEX, body=COMMUNITY_QUERY)
    # print("Got %d Hits:" % res['hits']['total'])
    for hit in res['aggregations']['repos']['buckets']:
        for dep in deplist:
            if 'pkg_age' not in dep:
                if hit['key'] in dep['repository']:
                    dep['commits_alltime'] = hit['commits']['value']
                    dep['commiters_alltime'] = hit['commiters']['value']
                    dep['last_act'] = hit['max_commit_date']['value']
                    dep['pkg_age'] = hit['min_commit_date']['value']
                    # Don't break because it can be repeated
                    # break
    # Last year
    COMMUNITY_QUERY["query"]["bool"]["must"][2]["range"]["grimoire_creation_date"]["gte"] = int(
        (datetime.now() - relativedelta(
            years=1)).timestamp() * 1000)
    res = es.search(index=ES_INDEX, body=COMMUNITY_QUERY)
    # print("Got %d Hits:" % res['hits']['total'])
    for hit in res['aggregations']['repos']['buckets']:
        for dep in deplist:
            if 'commits_lastyear' not in dep:
                if hit['key'] in dep['repository']:
                    dep['commits_lastyear'] = hit['commits']['value']
                    dep['commiters_lastyear'] = hit['commiters']['value']
    
    COMMUNITY_QUERY["query"]["bool"]["must"][2]["range"]["grimoire_creation_date"]["gte"] = int(
        (datetime.now() - relativedelta(
            months=6)).timestamp() * 1000)
    res = es.search(index=ES_INDEX, body=COMMUNITY_QUERY)
    # print("Got %d Hits:" % res['hits']['total'])
    for hit in res['aggregations']['repos']['buckets']:
        for dep in deplist:
            if 'commits_last6m' not in dep:
                if hit['key'] in dep['repository']:
                    dep['commits_last6m'] = hit['commits']['value']
                    dep['commiters_last6m'] = hit['commiters']['value']



def annotateduplicates(deplist):
    duplicates_same_version = {}
    duplicates_dif_version = {}
    for dep in deplist:
        if dep['id'] in duplicates_same_version:
            duplicates_same_version[dep['id']] += 1
        else:
            duplicates_same_version[dep['id']] = 1
        
        if dep['name'] in duplicates_dif_version:
            duplicates_dif_version[dep['name']]['total_times_appears'] += 1
            if dep['version'] not in duplicates_dif_version[dep['name']]['versions']:
                duplicates_dif_version[dep['name']]['versions'].append(dep['version'])
                duplicates_dif_version[dep['name']]['times_installed'] += 1
        else:
            duplicates_dif_version[dep['name']] = {
                'versions': [dep['version']],
                'times_installed': 1,
                'total_times_appears': 1
            }
    
    # Add the data
    for dep in deplist:
        dep['timesInstalled'] = duplicates_dif_version[dep['name']]['times_installed']
        dep['timesAppears'] = duplicates_dif_version[dep['name']]['total_times_appears']
        dep['timesAppearsSameId'] = duplicates_same_version[dep['id']]


def createprojectjson(repolist):
    projectjsongrimoirelab = {
        "babiaxr": {
            "meta": {
                "title": "BabiaXRDependenciesAnalisys"
            },
            "git": [],
            "github": [],
            "gitlab:issues": [],
            "gitlab:mrs": []
        }
    }
    
    for repo in repolist:
        projectjsongrimoirelab["babiaxr"]["git"].append("https://{}.git".format(repo))
        if repo.startswith("github"):
            projectjsongrimoirelab["babiaxr"]["github"].append("https://{}".format(repo))
        elif repo.startswith("gitlab"):
            projectjsongrimoirelab["babiaxr"]["gitlab:issues"].append("https://{}".format(repo))
            projectjsongrimoirelab["babiaxr"]["gitlab:mrs"].append("https://{}".format(repo))
    
    return projectjsongrimoirelab

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        s = "\n\nReceived Ctrl-C or other break signal. Exiting.\n"
        sys.stdout.write(s)
        sys.exit(0)


