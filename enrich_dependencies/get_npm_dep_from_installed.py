import argparse
import os
import sys
import time

import pandas as pd
import numpy as np
import requests
import json
from datetime import datetime
from pygount import SourceAnalysis, ProjectSummary
from glob import glob
from pathlib import Path
from elasticsearch import Elasticsearch

REPODB = {}

def main():
    global REPODB
    args = parse_args()
    
    if args.repos_db:
        repos = open(args.repos_db)
        REPODB = json.load(repos)
    
    # Open list
    installedlist = open(args.npm_list)
    npmlist = json.load(installedlist)

    deplist_to_enrich = []
    # Add the top app
    if 'version' in npmlist:
        version = npmlist['version']
    else:
        version = "latest"
    depid = "{}@{}".format(npmlist['name'], version)
    deplist_to_enrich.append({
        'name': npmlist['name'],
        'depName': npmlist['name'],
        'version': version,
        'id': depid,
        'installed_path': npmlist['path'],
        'parentsPath': "/{}/{}".format(npmlist['name'].replace("/", "-"), depid.replace("/", "-")),
        'noMetric': 1,
        'level': 0
    })
    gothroughtdeps(npmlist['dependencies'], deplist_to_enrich, "/{}".format(npmlist['name'].replace("/", "-")), 0)
    
    # Enrich with the npm registry
    repolist = []
    enrichwithnpmregistry(deplist_to_enrich, repolist)
    if args.repos_db:
        with open(args.repos_db, "w") as outfile:
            json.dump(REPODB, outfile)
    
    # Export projects.json for grimoirelab docker
    projectsjson = createprojectjson(repolist)
    with open(args.pjson_out, "w") as outfile:
        json.dump(projectsjson, outfile)
        
    # Save por si acaso
    with open(args.dep_output, "w") as outfile:
        json.dump(deplist_to_enrich, outfile)
    
    # Enrich with the physical sizes
    enrichwithphysical(deplist_to_enrich)
    
    # Annotate duplicates
    annotateduplicates(deplist_to_enrich)
    
    print(deplist_to_enrich)
    with open(args.dep_output, "w") as outfile:
        json.dump(deplist_to_enrich, outfile)
    
    

def parse_args():
    parser = argparse.ArgumentParser(usage="usage: enrich_dependencies.py [options]",
                                     description="Enrich metrics for representing the tree hierarchy")
    parser.add_argument("-depoutput", "--dep-output", required=False,
                        help="Dependency output file", default="./depout.json")
    parser.add_argument("-pjson", "--pjson-out", required=False,
                        help="Dependency output file", default="./projects.json")
    parser.add_argument("-npmlist", "--npm-list", required=True,
                        help="NPM dependencies list file", default="./npm_list.json")
    parser.add_argument("-reposdb", "--repos-db", required=False,
                        help="Repos database file", default="./npm_list.json")
    
    return parser.parse_args()


def gothroughtdeps(tree, deplist, current_path, level):
    level = level + 1
    for k, v in tree.items():
        # Check if empty
        if v:
            if 'version' in v:
                version = v['version']
            else:
                version = "latest"
            
            depid = "{}@{}".format(k, version)
            dependency = {
                'name': k, # This will be overwritten in the treebuilder component
                'depName': k,
                'version': version,
                'id': depid,
                'installed_path': v['path'],
                'parentsPath': "{}/{}/{}".format(current_path, k.replace("/", "-"), depid.replace("/", "-")),
                'noMetric': 1,
                'level': level
            }
            deplist.append(dependency)
            
            if 'dependencies' in v:
                gothroughtdeps(v['dependencies'], deplist, '{}/{}'.format(current_path, k.replace("/", "-")), level)
        
        
def enrichwithnpmregistry(deplist, repolist):
    already_fetched = {}
    repo_inserted_by_hand = {}
    for dep in deplist:
        # Not needed to go to the registry again
        if dep['id'] in already_fetched:
            print("Not needed to go to the registry, I already have the data {}".format(dep['id']))
            save_dep_data(dep, already_fetched[dep['id']], repolist, repo_inserted_by_hand)
        # Then needed to query
        else:
            print("Going to the registry for {}".format(dep['id']))
            x = requests.get('https://registry.npmjs.org/{}/{}'.format(dep['name'], dep['version']))
            if x.status_code == 200:
                resp = x.json()
                already_fetched[dep['id']] = resp
                save_dep_data(dep, resp, repolist, repo_inserted_by_hand)
            else:
                print("Something wrong happened with {}".format(dep['id']), x.status_code)
                insert_repo_by_hand(dep, repolist, repo_inserted_by_hand)
                dep['license'] = "UNKNOWN"


def save_dep_data(dep, resp, repolist, repobyhand):
    global REPODB
    # License
    if 'license' in resp:
        dep['license'] = resp['license']
    else:
        dep['license'] = "UNKNOWN"
    
    # Repository
    if 'repository' in resp:
        if 'url' in resp['repository']:
            repo_clean = resp['repository']['url'].split("git@")[-1].split("git+")[-1].split("//")[-1].split("/tree/")[0]
            if ".git" in repo_clean:
                repo_clean = repo_clean.split(".git")[0]
                
            # Check if in database
            if dep['id'] not in REPODB.keys():
                # Casos barrera
                if ":" in repo_clean:
                    insert_repo_by_hand(dep, repolist, repobyhand)
                else:
                    # Check if exist on GitHub
                    x = requests.get('https://github.com/{}'.format(repo_clean.split("github.com/")[-1]))
                    if x.status_code == 200:
                        dep['repository'] = repo_clean
                        if repo_clean not in repolist:
                            repolist.append(repo_clean)
                    else:
                        insert_repo_by_hand(dep, repolist, repobyhand)
            else:
                print("{} repo already in REPODB database".format(dep['id']))
                dep['repository'] = REPODB[dep['id']]
                repolist.append(dep['repository'])
        else:
            insert_repo_by_hand(dep, repolist, repobyhand)
    else:
        insert_repo_by_hand(dep, repolist, repobyhand)

    # Size and physical metrics
    if 'dist' in resp:
        if 'unpackedSize' in resp['dist']:
            dep['size'] = resp['dist']['unpackedSize']
            

def insert_repo_by_hand(dep, repolist, repobyhand):
    global REPODB
    if not dep['id'] in repobyhand.keys():
        if not dep['id'] in REPODB.keys():
            repo = input("Impossible to find the repo of {}, insert it manualy (without https://): ".format(dep['id']))
            dep['repository'] = repo
            repobyhand[dep['id']] = repo
            REPODB[dep['id']] = repo
            repolist.append(repo)
        else:
            dep['repository'] = REPODB[dep['id']]
    else:
        dep['repository'] = repobyhand[dep['id']]


def enrichwithphysical(deplist):
    # Size and loc, and save in memory when analyzed first time
    paths_analyzed = {}
    for dep in deplist:
        print("Analizing physical and size for {}".format(dep['id']))
        if dep['id'] in paths_analyzed:
            if ('size' not in dep) and ('size' in paths_analyzed[dep['id']]):
                dep['size'] = paths_analyzed[dep['id']]['size']
            elif ('size' not in dep) and ('size' not in paths_analyzed[dep['id']]):
                dep['size'] = os.path.getsize(str(dep['installed_path']))
                paths_analyzed[dep['id']]['size'] = dep['size']
            
            if 'loc' in paths_analyzed[dep['id']]:
                dep['loc'] = paths_analyzed[dep['id']]['loc']
            else:
                get_loc(dep, paths_analyzed)
                
        else:
            paths_analyzed[dep['id']] = {}
            if 'size' not in dep:
                dep['size'] = os.path.getsize(str(dep['installed_path']))
                paths_analyzed[dep['id']]['size'] = dep['size']
            get_loc(dep, paths_analyzed)
            

def get_loc(dep, paths_analyzed):
    for path in Path(dep['installed_path']).rglob('*.js'):
        # Check if file only and not inside its node_modules
        if "node_modules" not in str(path).split(dep['installed_path'])[-1]:
            if os.path.isfile(path):
                source_analysis = SourceAnalysis.from_file(path, "pygount")
                if 'loc' not in dep:
                    dep['loc'] = 0
                dep['loc'] = dep['loc'] + source_analysis.code_count
        
    for path in Path(dep['installed_path']).rglob('*.ts'):
        # Check if file only and not inside its node_modules
        if "node_modules" not in str(path).split(dep['installed_path'])[-1]:
            if os.path.isfile(path):
                source_analysis = SourceAnalysis.from_file(path, "pygount")
                if 'loc' not in dep:
                    dep['loc'] = 0
                dep['loc'] = dep['loc'] + source_analysis.code_count

    # no code
    if 'loc' not in dep:
        dep['loc'] = 0
    paths_analyzed[dep['id']]['loc'] = dep['loc']
    

def annotateduplicates(deplist):
    duplicates_same_version = {}
    duplicates_dif_version = {}
    for dep in deplist:
        if dep['id'] in duplicates_same_version:
            duplicates_same_version[dep['id']] += 1
        else:
            duplicates_same_version[dep['id']] = 1
        
        if dep['name'] in duplicates_dif_version:
            duplicates_dif_version[dep['name']]['total_times_appears'] += 1
            if dep['version'] not in duplicates_dif_version[dep['name']]['versions']:
                duplicates_dif_version[dep['name']]['versions'].append(dep['version'])
                duplicates_dif_version[dep['name']]['times_installed'] += 1
        else:
            duplicates_dif_version[dep['name']] = {
                'versions': [dep['version']],
                'times_installed': 1,
                'total_times_appears': 1
            }
    
    # Add the data
    for dep in deplist:
       dep['timesInstalled'] = duplicates_dif_version[dep['name']]['times_installed']
       dep['timesAppears'] = duplicates_dif_version[dep['name']]['total_times_appears']
       dep['timesAppearsSameId'] = duplicates_same_version[dep['id']]
       
 
def createprojectjson(repolist):
    projectjsongrimoirelab = {
        "babiaxr": {
            "meta": {
                "title": "BabiaXRDependenciesAnalisys"
            },
            "git": [],
            "github": [],
            "gitlab:issues": [],
            "gitlab:mrs": []
        }
    }
    
    for repo in repolist:
        projectjsongrimoirelab["babiaxr"]["git"].append("https://{}.git".format(repo))
        if repo.startswith("github"):
            projectjsongrimoirelab["babiaxr"]["github"].append("https://{}".format(repo))
        elif repo.startswith("gitlab"):
            projectjsongrimoirelab["babiaxr"]["gitlab:issues"].append("https://{}".format(repo))
            projectjsongrimoirelab["babiaxr"]["gitlab:mrs"].append("https://{}".format(repo))
        
    return projectjsongrimoirelab
        
if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        s = "\n\nReceived Ctrl-C or other break signal. Exiting.\n"
        sys.stdout.write(s)
        sys.exit(0)

