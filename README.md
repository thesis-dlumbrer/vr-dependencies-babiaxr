# Visualizing NPM dependencies

# 1. Install your NPM package/application


```
npm install --omit=dev <package_name>
```

# 2. Get the dependencies list and the vulnerabilites

## Dependencies list

```
npm ls --all --long --omit=dev --json > npm_list.json 
```

Save the path of the file!

## Vulnerabilites

```
npm audit --omit=dev --json > npm_audit.json
```

Save the path of the file!

# 3. Get the metrics from the NPM registry

In this step we are going to fetch all the information (size, license, repo url...) of the package in the NPM registry, if available.

1. Optional: create a python3 virtual env

```
python3 -m venv /tmp/vrdependencies 
source /tmp/vrdependencies/bin/activate
```

2. Install requirements

```
pip3 install -r requirements.txt
```

3. Execute the code
```
cd enrich_dependencies
python3 get_npm_dep_from_installed.py -npmlist npm_list.json -depoutput <newlist.json> -pjson <projects.json>
```


### Fix URLs that couldn't be found

If there is a repository that couldn't found its url, the script will ask for writting it manually (or leave it blank)

Example:
```
REPO URL NOT FOUND for pkgparent/pkg@version. Please, fill it manually or leave it blank: https://github.com/repourl

```

### Returned files

This python code will return two important files for the next step:

- `newlist.json`: a simple 1D list with all the dependencies
- `projects.json`: project.json file for analyze with GrimoireLab



# 4. Get the community metrics from the pacakge repository

Using GrimoreLab!

### GrimoireLab docker images

1. Go to `grimoirelab-docker/default-grimoirelab-settings` and replace the content of `projects.json` file for the generated in the previous step.
2. Go to `grimoirelab-docker/compose` and launch the docker-compose, wait until all the git data is fetched, inside the /tmp/ folder the logs can be visited.



# 5. Enrich the final dataset with community metrics, size metrics and github issues/prs metrics.

**Important: container from GrimoireLab should be up!**

This step is optional, to execute it, you have to go to the `enrich_dependencies` folder and:

**You can use the python venv created before**

1. Run the file
```
python3 enrich_dependencies.py 
-deplist <dependencies_list_json>
-depout <dependencies_list_enriches_output_path>
-auditfile <npm_audit_file>
-githubusername <github_username>
-githubtoken <github_token>
```

2. It will take a bit due to the GitHub API rate limit, once finished, the returned file will be in the `<dependencies_list_enriches_output_path>` path ready to use in the city visualization.